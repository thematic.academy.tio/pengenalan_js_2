var input = document.getElementById('input');

var count;

input.onkeyup = function () {
    count = parseInt(input.value);
};

function plus10() {
    count = count + 10;
    input.value = count;
}

function plus1() {
    count++;
    input.value = count;
}

function min1() {
    count--;
    input.value = count;
}

function min10() {
    count = count - 10;
    input.value = count;
}

console.log(count);
